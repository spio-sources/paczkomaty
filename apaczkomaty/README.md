# Paczkomaty

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.
Next to the basic features you will find a bit of real frontend, to check how the project works.

## Development server

Run `ng serve` or `yarn start` for a dev server. Navigate to `http://localhost:4000/`. The app will automatically reload if you change any of the source files.

Run `ng build` or `yarn build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Docker
It's possible to run development server with `docker-compose up` command, which will build container and spin it up. Application is also exposed at port `:4000`, but be careful with IP address (on unix-like systems it will be localhost:4000 but on windows it can be 192....:4000).
