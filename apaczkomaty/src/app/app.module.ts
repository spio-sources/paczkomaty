import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PricesComponent } from './price/prices.component';
import { MachinesComponent } from './machine/machines.component';
import { PriceService } from './price/price-service';
import { PriceStrategyFactory } from './price/price-strategy-factory';
import { BoxGroupBuilder } from './box/box-group-builder';
import { MachineQuantitiesComponent } from './machine/machine-quantities.component';
import { PriceSelectorComponent } from './price/price-selector.component';
import { PackOperationsComponent } from './machine/pack-operations.component';
import { RuntimeIdGenerator } from './id/runtime-id-generator';
import { IdGenerator } from './id/id-generator';
import { IdComponent } from './id/id.component';

const appRoutes: Routes = [
  {path: 'prices', component: PricesComponent},
  {path: 'id', component: IdComponent},
  {path: 'machines', component: MachinesComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    PricesComponent,
    MachinesComponent,
    MachineQuantitiesComponent,
    PriceSelectorComponent,
    PackOperationsComponent,
    IdComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule
  ],
  providers: [PriceService, PriceStrategyFactory, BoxGroupBuilder, {
    provide: IdGenerator,
    useClass: RuntimeIdGenerator
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
