import { Injectable } from '@angular/core';
import { IdGenerator } from './id-generator';

@Injectable()
export class RuntimeIdGenerator implements IdGenerator {

    getPackMachineId(): string {
        return '';
    }

    getPackId(): string {
        return '';
    }

    getTransportId(): string {
        return '';
    }
}