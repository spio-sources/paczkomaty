import { Component, Output } from '@angular/core';
import { PackSize } from '../pack/pack-size';
import { Subject } from 'rxjs';

@Component({
    selector: 'price-selector',
    template: `
        <div class="price-group">
            <ng-container *ngFor="let item of sizes">
                <input  type="radio"
                        name="price"
                        id="price-{{item}}"
                        (change)="onSizeSelected(item)">
                <label for="price-{{item}}">{{item}}</label>
            </ng-container>
        </div>
    `
})
export class PriceSelectorComponent {
    sizes = Object.keys(PackSize);

    @Output()
    packSize: Subject<PackSize> = new Subject<PackSize>();

    onSizeSelected(item: PackSize): void {
        this.packSize.next(item);
    }
}