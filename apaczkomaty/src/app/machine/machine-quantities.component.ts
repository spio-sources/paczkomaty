import { Component, Input, Output } from '@angular/core';
import { PackMachine } from './pack-machine';
import { MachineFreeSpaceVisitor } from './machine-free-space-visitor';
import { PackSize } from '../pack/pack-size';

@Component({
    selector: 'quantities',
    template: `
    <div class="module">
        <label>Weryfikacja zajętości </label>
        <input type="button" value="Zbadaj pojemność" (click)="countQuantities()">
        <textarea>{{ asJson(quantities) }}</textarea>
    </div>
    `
})
export class MachineQuantitiesComponent {

    @Input()
    packMachine: PackMachine;
    quantities: { [key in PackSize]: number };

    countQuantities(): void {
        const visitor = new MachineFreeSpaceVisitor();
        this.packMachine.accept(visitor);
        this.quantities = visitor.quantities;
    }

    asJson(object: any): string {
        return JSON.stringify(object);
    }

}