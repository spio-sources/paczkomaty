import { Box } from '../box/box';
import { BoxGroup } from '../box/box-group';
import { PackMachine } from './pack-machine';

export interface MachineVisitor {
    visitBox(box: Box): void;
    visitBoxGroup(boxGroup: BoxGroup): void;
    visitPackMachine(packMachine: PackMachine): void;
}