import { MachineVisitor } from './machine-visitor';
import { Box } from '../box/box';
import { BoxGroup } from '../box/box-group';
import { PackMachine } from './pack-machine';
import { PackSize } from '../pack/pack-size';

export class MachineFreeSpaceVisitor implements MachineVisitor {

    quantities: { [key in PackSize]: number } = { A: 0, B: 0, C: 0 };

    visitBox(box: Box): void {
    }

    visitBoxGroup(boxGroup: BoxGroup): void {
    }

    visitPackMachine(packMachine: PackMachine): void {
    }
}
